# inherit-with-extend

Имеется два шаблона джобы (для простоты, пусть первая выводит 1, а вторая выводит 2)

### job-1.yml
```
.build: &build
  stage: build
  image: alpine:3
  script:
    - echo 1
```

### job-2.yml
```
.build: &build
  stage: build
  image: alpine:3
  script:
    - echo 2
```

Что будет если мы их оба заинклюдим?
Что мы получим 1 или 2?

**`Выполнится та джоба, которая инклюдилась последней`**